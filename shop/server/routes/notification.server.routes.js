'use strict';

module.exports = function(app, appslug) {

  var path = require('path');
  // var hrvMiddleware = require(path.resolve('./embed-modules/core/server/middlewares/core-haravan-api.server.middleware'));
  var itemController = require('../controllers/notification.server.controller');
  console.log("appslug")
  console.log(appslug)
  // Define application route
  //app.route('/' + appslug + '/api/item')
  //    .get(hrvMiddleware.authenticateApi, itemController.getCollect);
  //create data
  app.route('/' + appslug + '/api/getUser')
      .get(itemController.getItem, itemController.read);

  app.route('/' + appslug + '/api/getAllNotif')
      .get(itemController.getAllNotif, itemController.read);

  // app.route('/' + appslug + '/api/createUser')
  //     .post(hrvMiddleware.authenticateApi, itemController.createItem);
  //
  // app.route('/' + appslug + '/api/updateUser')
  //     .post(hrvMiddleware.authenticateApi, itemController.updateItem);

  app.route('/' + appslug + '/api/getStoreId')
      .get(itemController.getStoreId);

};

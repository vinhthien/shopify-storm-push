'use strict';

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var Schema = mongoose.Schema;

var item = require('../models/user.server.model');
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Payment';
var itemModel = mongoose.model(prefix);

exports.createOrUpdatePayment = function (item,callback) {
  itemModel.createOrUpdatePayment(item,function () {
      callback();
  });
};

exports.getPayment = function (shop,callback) {
    itemModel.getPayment(shop,function (err, data) {
        callback(data);
    });
};

'use strict';

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var Schema = mongoose.Schema;
var MainServiceUtil = require("../../../utility/app.service.utils");

var item = require('../models/user.server.model');
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Notification';
var itemModel = mongoose.model(prefix);

exports.createItem = function (req, res) {
    itemModel.createUser(req.query, function (err) {
        if (err) {
            res.json(err);
        }
    })
    res.json({err: false, message: 'successfully'});
};

exports.createNotification = function (obj, callback) {
    itemModel.createNotification(obj, function (notification) {
            callback(notification);
    })
};

exports.updateNotification = function (obj, callback) {
    itemModel.updateNotification(obj, function () {
        if(callback){
            callback();
        }
    })
};

exports.duplicateNotification = function (obj, callback) {
    obj.repeat += 1;
    itemModel.updateNotification(obj, function () {
        callback();
    })
};

exports.deleteNotification = function (id, callback) {
    itemModel.deleteNotification(id, function () {
        callback();
    })

};

exports.read = function (req, res) {
    console.log("IN as")
    if (req.model !== '') {
        res.json(req.model)
    } else {
        res.json({store_id: '', err: true, message: 'cannot collect'});
    }

};

exports.getItem = function (req, res, next) {
    // itemModel.getitem({ finger_print: req.query.finger_print,
    //     shop: req.query.shop}, function (err, foundItem) {
    //   if (err) {
    //     return next(err);
    //   } else if (!foundItem) {
    //     req.model = '';
    //     return next();
    //   }
    //   req.model = foundItem;
    //   next();
    // });

    itemModel.findOne({
        finger_print: req.query.finger_print,
        shop: req.query.shop
    }, function (err, result) {
        if (err) {
            return next(err);
        } else if (!result) {
            req.model = '';
            return next();
        }
        req.model = result;
        next();
    });
};

exports.findUserByInfo = function (finger_print, shop, callback) {
    // itemModel.getitem({ finger_print: req.query.finger_print,
    //     shop: req.query.shop}, function (err, foundItem) {
    //   if (err) {
    //     return next(err);
    //   } else if (!foundItem) {
    //     req.model = '';
    //     return next();
    //   }
    //   req.model = foundItem;
    //   next();
    // });

    itemModel.findOne({
        finger_print: finger_print,
        shop: shop
    }, function (err, result) {
        if (err) {
            callback(err);
            return;
        } else if (!result) {
            callback(err);
            return;
        }
        callback(false, result);
    });
};

exports.getAllNotif = function (req, res) {
    var shop = req.query.shop;
    itemModel.getNotifByShop(shop, function (err, result) {
        if (err) {
            res.json([]);
        } else if (!result) {
            res.json([]);
        }
        res.json(result);
    });
};

exports.getAllNotificationByShop = function (shop,callback) {
    itemModel.getNotificationByShop(shop, function (err, result) {
        if (err) {
            return callback([]);
        } else if (!result) {
            return callback([]);
        }
        callback(result);
    });
};

exports.getAllNotificationById = function (id,callback) {
    itemModel.getNotificationById(id, function (err, result) {
        if (err) {
            return callback([]);
        } else if (!result) {
            return callback([]);
        }
        callback(result);
    });
};

exports.getStoreId = function (req, res) {
    return res.json({store_id: req.session.shop});
};



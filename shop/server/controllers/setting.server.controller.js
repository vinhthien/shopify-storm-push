'use strict';

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var Schema = mongoose.Schema;

var item = require('../models/user.server.model');
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Setting';
var itemModel = mongoose.model(prefix);

exports.saveSetting = function (item,callback) {
  itemModel.createSetting(item,function () {
      callback();
  });
};

exports.getSetting = function (shop,callback) {
    itemModel.getSetting(shop,function (err, data) {
        callback(data);
    });
};

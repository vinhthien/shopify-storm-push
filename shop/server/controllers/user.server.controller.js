'use strict';

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var Schema = mongoose.Schema;

var item = require('../models/user.server.model');
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'User';
var itemModel = mongoose.model(prefix);

exports.createItem = function (req, res) {
  itemModel.createUser(req.query, function (err) {
    if (err) {
      res.json(err);
    }
  })
  res.json({err: false, message: 'successfully'});
};

exports.createUser = function (obj,callback) {
    itemModel.createUser(obj, function (err) {
        if (err) {
            callback(err);
        }
    })
    callback({err: false, message: 'successfully'});
};

exports.updateItem = function (req, res) {
  itemModel.updateitem(req.query, function (err) {
    if (err) {
      res.json(err);
    }
  })
  res.json({err: false, message: 'successfully'});
};

exports.updateUser = function (obj,callback) {
    itemModel.updateitem(obj, function (err) {
        if (err) {
            callback(err);
        }
    })
    callback({err: false, message: 'successfully'});
};

exports.read = function (req, res) {
  if(req.model !== ''){
    res.json(req.model)
  } else{
    res.json({store_id: '', err: true, message: 'cannot collect'});  
  }
  
};

exports.getItem = function (req, res, next) {
  // itemModel.getitem({ finger_print: req.query.finger_print,
  //     shop: req.query.shop}, function (err, foundItem) {
  //   if (err) {
  //     return next(err);
  //   } else if (!foundItem) {
  //     req.model = '';
  //     return next();
  //   }
  //   req.model = foundItem;
  //   next();
  // });

    itemModel.findOne({ finger_print: req.query.finger_print,
        shop: req.query.shop}, function (err, result){
      if (err) {
            return next(err);
        } else if (!result) {
            req.model = '';
            return next();
        }
        req.model = result;
        next();
    });
};

exports.findUserByInfo = function (finger_print,shop,callback) {
    // itemModel.getitem({ finger_print: req.query.finger_print,
    //     shop: req.query.shop}, function (err, foundItem) {
    //   if (err) {
    //     return next(err);
    //   } else if (!foundItem) {
    //     req.model = '';
    //     return next();
    //   }
    //   req.model = foundItem;
    //   next();
    // });

    itemModel.findOne({ finger_print: finger_print,
        shop: shop}, function (err, result){
        if (err) {
            callback(err);
            return;
        } else if (!result) {
            callback(err);
            return;
        }
        callback(false,result);
    });
};

exports.getAllUser = function (shop,callback) {

    itemModel.find({
        shop: shop
    }, function (err, result){
        if (err) {
            callback([]);
        } else if (!result) {
          callback([]);
        }
        callback(result);
    });
};


exports.getStoreId = function (req, res) {
  return res.json({store_id: req.session.shop});
};



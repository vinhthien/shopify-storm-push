'use strict';

var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var Schema = mongoose.Schema;
var MainServiceUtil = require("../../../utility/app.service.utils");

var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Shop';
var itemModel = mongoose.model(prefix);

exports.createShop = function (shop, dataInfo, callback) {
    itemModel.createShop(shop, dataInfo, callback);
};

exports.updateShopByWebhook = function (shopInfo,callback) {
    itemModel.updateShopByWebhook(shopInfo,callback);
};

exports.getAllShop = function (callback) {
    itemModel.getAllShop(callback);
};

exports.getShopInfo = function (shop,callback) {
    itemModel.getShopInfo(shop,callback);
};







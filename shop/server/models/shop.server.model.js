'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Schema = mongoose.Schema;
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Shop';

/**
 * Shop Schema
 */
var UserSchema = new Schema({
    // _id: { type: String, required: true, index: { unique: true } },
    shop: String,
    shopify_settings: { type: Schema.Types.Mixed }
});


UserSchema.statics.prefix = prefix;


UserSchema.statics.getAllShop = function(callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    var criteria = {};
    ItemModel.find(criteria).exec(callback);
};

UserSchema.statics.createShop = function (shop, dataInfo,callback) {

    var item  = {
        shop: shop,
        shopify_settings: dataInfo
    }
    var ItemModel = mongoose.model(prefix, UserSchema);

    ItemModel.findOne({
        shop: shop
    }, function (err, result) {
        if (err) return err;
        if (result !== null) {
            result.set(item);
            result.save();
            callback(result);
        } else {
            var user = new ItemModel(item);
            user.save(function (err) {
                if (err) return err;
                callback(item);
            });
        }
    });
};

UserSchema.statics.getShopInfo = function (shop,callback) {


    var ItemModel = mongoose.model(prefix, UserSchema);

    ItemModel.findOne({
        shop: shop
    }, function (err, result) {
        if (err) return err;
        if (result !== null) {
            callback(result);
        }
    });
};


UserSchema.statics.updateShopByWebhook = function (shopInfo,callback) {


    var ItemModel = mongoose.model(prefix, UserSchema);

    ItemModel.findOne({
                "shopify_settings.shop.id": shopInfo.id
    }, function (err, result) {
        if (err) {
            return err;
        }
        if (result !== null) {
            console.log("result")
            console.log(result)
            var item = result;
            item.shopify_settings.shop = shopInfo;
            ItemModel.findOneAndUpdate({shop: result.shop}, item, function (err, place) {
                callback(item);
            });
        }
    });
};


mongoose.model(prefix, UserSchema);


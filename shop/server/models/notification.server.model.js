'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Schema = mongoose.Schema;
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Notification';

/**
 * Shop Schema
 */
var UserSchema = new Schema({
    shop: String,
    title: String,
    description: String,
    icon: String,
    image: String,
    link: String,
    firstCTAText: String,
    firstCTALink: String,
    firstCTAIcon: String,
    enableFirstCTA: Boolean,
    secondCTAText: String,
    secondCTALink: String,
    secondCTAIcon: String,
    enableSecondCTA: Boolean,
    type: String,
    send: Number,
    click: Number,
    order: String,
    revenueNumber: Number,
    revenueText: String,
    repeat: Number,
    createdAt: String
});

UserSchema.statics.prefix = prefix;

UserSchema.statics.createNotification = function(item, callback) {
  var ItemModel = mongoose.model(prefix, UserSchema);
  var user = new ItemModel(item);
  user.save(function (err,notification) {
      if (err) return err;
      if(callback){
          callback(notification);
      }
      // var newRoomId = notif._id;

  });
  
};

UserSchema.statics.updateNotification = function(item, callback) {
  	
  	var ItemModel = mongoose.model(prefix, UserSchema);

	ItemModel.findOne({ _id: item._id}, function (err, result){
		if (err) return err;
		if(result !== null){
			result.set(item);
	  	result.save();
		}
		callback();
	});
};

UserSchema.statics.getitem = function(item, callback) {
	var ItemModel = mongoose.model(prefix, UserSchema);
  var criteria = {
      finger_print: item.finger_print,
      shop: item.shop,
  };
  ItemModel.findOne(criteria).exec(callback);
};


UserSchema.statics.getNotificationByShop = function(shop, callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    var criteria = {shop: shop};

    ItemModel.find(criteria).exec(callback);
};

UserSchema.statics.getNotificationById = function(id, callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    var criteria = {_id: id};

    ItemModel.find(criteria).exec(callback);
};

UserSchema.statics.deleteNotification = function(id, callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    ItemModel.deleteOne({ _id: id }, function (err) {
        if (err) return;
        callback();
        // deleted at most one tank document
    });
};


mongoose.model(prefix, UserSchema);


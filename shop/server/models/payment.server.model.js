'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Schema = mongoose.Schema;
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Payment';

/**
 * Shop Schema
 */
var UserSchema = new Schema({
    shop: String,
    hasPayOff: Boolean
});


UserSchema.statics.prefix = prefix;

UserSchema.statics.createOrUpdatePayment = function(item,callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);

    ItemModel.findOne({ shop: item.shop}, function (err, result){
        if (err) return err;
        if(result !== null){
            result.set(item);
            result.save();
            callback();
        }else {
            var user = new ItemModel(item);
            user.save(function (err) {
                if (err) return err;
                callback();
            });
        }
    });
};

UserSchema.statics.getPayment = function(shop, callback) {
	var ItemModel = mongoose.model(prefix, UserSchema);
  var criteria = {
      shop: shop
  };
  ItemModel.find(criteria).exec(callback);
};

mongoose.model(prefix, UserSchema);


'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Schema = mongoose.Schema;
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'User';

/**
 * Shop Schema
 */
var UserSchema = new Schema({
    finger_print: String,
    shop: String,
    token: String,
    browser_name: String
});

UserSchema.statics.prefix = prefix;

UserSchema.statics.createUser = function(item, callback) {
  console.log("item")
  console.log(item)
  var ItemModel = mongoose.model(prefix, UserSchema);
  var user = new ItemModel(item);
  user.save(function (err) {
      console.log("err")
      console.log(err)
      if (err) return err;
   });
  
};

UserSchema.statics.updateitem = function(item, callback) {
  	
  	var ItemModel = mongoose.model(prefix, UserSchema);

	ItemModel.findOne({ finger_print: item.finger_print,
        shop: item.shop}, function (err, result){
		if (err) return err;
		if(result !== null){
			result.set(item);
	  	result.save();
		}
	});
};

UserSchema.statics.getitem = function(item, callback) {
	var ItemModel = mongoose.model(prefix, UserSchema);
  var criteria = {
      finger_print: item.finger_print,
      shop: item.shop,
  };
  ItemModel.findOne(criteria).exec(callback);
};


UserSchema.statics.getallitem = function(item, callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    var criteria = {};
    ItemModel.find(criteria).exec(callback);
};


mongoose.model(prefix, UserSchema);


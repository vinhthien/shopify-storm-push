'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var path = require('path');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));

var Schema = mongoose.Schema;
var prefix = process.env.SHOPIFY_DATABASE_PREFIX + '-' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '-' + 'Push';

/**
 * Shop Schema
 */
var UserSchema = new Schema({
    shop: String,
    title: String,
    description: String,
    icon: String,
    image: String,
    link: String,
    firstCTAText: String,
    firstCTALink: String,
    firstCTAIcon: String,
    enableFirstCTA: Boolean,
    secondCTAText: String,
    secondCTALink: String,
    secondCTAIcon: String,
    enableSecondCTA: Boolean,
    type: String
});


UserSchema.statics.prefix = prefix;

UserSchema.statics.createPush = function (item) {
    var ItemModel = mongoose.model(prefix, UserSchema);

    ItemModel.findOne({
        shop: item.shop,
        type: item.type
    }, function (err, result) {
        if (err) return err;
        if (result !== null) {
            result.set(item);
            result.save();
        } else {
            var user = new ItemModel(item);
            user.save(function (err) {
                if (err) return err;
            });
        }
    });
};

UserSchema.statics.setPush = function (item, callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    ItemModel.findOne({
        shop: item.shop
    }, function (err, result) {
        if (err) return err;
        if (result !== null) {
            result.set(item);
            result.save();
        }
    });
};

UserSchema.statics.getitem = function (item, callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    var criteria = {
        finger_print: item.finger_print,
        shop: item.shop,
    };
    ItemModel.findOne(criteria).exec(callback);
};

UserSchema.statics.getNotifByShop = function (shop, callback) {
    var ItemModel = mongoose.model(prefix, UserSchema);
    var criteria = {shop: shop};
    ItemModel.find(criteria).exec(callback);
};

mongoose.model(prefix, UserSchema);


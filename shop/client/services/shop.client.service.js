'use strict';

angular
    .module('core')
    .factory('Shop', Shop);

Shop.$inject = [
  '$resource'
];

function Shop ($resource) {
  return $resource(
      appslug + '/api/shop',
      {
        shop: shopname,
        timestamp: timestamp,
        signature: signature,
        code: code
      },
      {
        query: {
          isArray: false
        },
        update: {
          method: 'PUT',
          isArray: false
        }
      }
  );
}

class Utils {
    static getNow() {
        var moment = require('moment');
        return moment().format();
    }

}

module.exports = Utils;
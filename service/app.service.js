var path = require('path');
var mongoose = require('mongoose');
var url = require('url');
var crypto = require('crypto');
const fs = require('fs');
const express = require('express');
var ShopMD = mongoose.model(process.env.HARAVAN_DATABASE_PREFIX + '-' + 'Shop');


const nonce = require('nonce')();
const cookie = require('cookie');
const querystring = require('querystring');
const request = require('request-promise');

var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var ShopAPI = require(path.resolve('./apis/haravan/haravan-api/server/api/shop/shop.server.api'));
var ScriptTagApi = require(path.resolve('./apis/haravan/haravan-api/server/api/script-tag/script-tag.server.api'));
var BillingApi = require(path.resolve('./apis/shopify/shopify-api/server/api/billing/billing.server.api'));
var UserController = require('../shop/server/controllers/user.server.controller');
var PushController = require(path.join(__dirname, '../shop/server/controllers/push.server.controller'));
var SettingController = require(path.join(__dirname, '../shop/server/controllers/setting.server.controller'));
var PaymentController = require(path.join(__dirname, '../shop/server/controllers/payment.server.controller'));
var ShopController = require(path.join(__dirname, '../shop/server/controllers/shop.server.controller'));
var NotificationController = require(path.join(__dirname, '../shop/server/controllers/notification.server.controller'));
var FirebaseProvider = require("../provider/firebase.provider");
var ApnProvider = require("../provider/apn.provider");
var S3Provider = require("../provider/upload.image.provider");
var MainServiceConstant = require("../utility/app.service.constants");
var MainServiceUtil = require("../utility/app.service.utils");
var MainService = require('../../share-lib/service/main.service');

exports.checkScriptTag = function (host, shop, accessToken) {
    var client_url = host + "/shopify-storm-push/lib/script-tags/push/js/storm-push-shopify-frontend.js";
    MainService.addScriptTag(client_url, shop, accessToken);
}

exports.initShopInfoWebhook = function (shop,accessToken) {
    var url = process.env.WEB_HOST + '/' + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '/shopWebhook';
    MainService.initShopInfoWebhook(shop, accessToken, url);
}

exports.getShopInfoForFinalize = function (accessToken, shop, callback) {
    MainService.getShopInfo(accessToken, shop, callback);
}

exports.processAuthorize = function (req,res) {
    const {shop, hmac, code, state} = req.query;
    const stateCookie = cookie.parse(req.headers.cookie).state;

    if (state !== stateCookie) {
        return res.status(403).send('Request origin cannot be verified');
    }

    if (shop && hmac && code) {
        // DONE: Validate request is from Shopify
        const map = Object.assign({}, req.query);
        delete map['signature'];
        delete map['hmac'];
        const message = querystring.stringify(map);
        const providedHmac = Buffer.from(hmac, 'utf-8');
        const generatedHash = Buffer.from(
            crypto
                .createHmac('sha256', process.env.SHOPIFY_STORM_PUSH_API_SECRET)
                .update(message)
                .digest('hex'),
            'utf-8'
        );
        let hashEquals = false;

        try {
            hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
        } catch (e) {
            hashEquals = false;
        }
        ;

        if (!hashEquals) {
            return res.status(400).send('HMAC validation failed');
        }

        // DONE: Exchange temporary code for a permanent access token
        const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token';
        const accessTokenPayload = {
            client_id: process.env.SHOPIFY_STORM_PUSH_API_KEY,
            client_secret: process.env.SHOPIFY_STORM_PUSH_API_SECRET,
            code,
        };

        request.post(accessTokenRequestUrl, {json: accessTokenPayload})
            .then((accessTokenResponse) => {
                const accessToken = accessTokenResponse.access_token;
                var domain = url.parse(process.env.WEB_HOST).hostname;
                var embed = process.env.SHOPIFY_STORM_PUSH_APP_SLUG;
                this.checkScriptTag(process.env.WEB_HOST, shop, accessToken);
                this.getShopInfoForFinalize(accessToken,shop,function (info) {
                    info = JSON.parse(info);
                    ShopController.createShop(shop,info,function (newInfo) {
                    });
                })
                this.initShopInfoWebhook(shop,accessToken);
                var billingApi = new BillingApi(accessToken,shop);
                billingApi.getAllBilling(function (billingList) {
                    if (isJsonString(billingList)) billingList = JSON.parse(billingList);
                    var array = billingList.recurring_application_charges;
                    if(array.length>0){
                        var status = array[0].status;
                        var id = array[0].id;
                        if(status == 'accepted'){billingApi.activeSubcription(id,function (result) {})}
                        // if(status == 'active'){billingApi.cancelSubcription(id,function (result) {})}
                    }
                });
                // PaymentController.createOrUpdatePayment({shop: shop,hasPayOff: false},function () {});
                res.render(path.join(__dirname, '../core/server/views/index'), {
                    embed: embed,
                    embedUrl: "",
                    domain: process.env.WEB_HOST,
                    shopname: shop,
                    timestamp: "",
                    signature: "",
                    code: "",
                    access_token: accessToken,
                    shop: "",
                    defaultIcon: process.env.WEB_HOST + process.env.SHOPIFY_STORM_PUSH_DEFAULT_ICON,
                    user: shop.replace(".myshopify.com", ""),
                    shopUrl: 'https://'+shop
                });
                // TODO
                // Use access token to make API call to 'shop' endpoint
            })
            .catch((error) => {
                res.status(error.statusCode).send(404);
            });

    } else {
        res.status(400).send('Required parameters missing');
    }
}

exports.getAllShop = function (shop,callback) {
    ShopController.getAllShop(function (err,result) {
        if (result && result.length > 0) {
            var white_list = [];
            white_list.push(process.env.WEB_HOST);
            for (var index = 0; index < result.length; index++) {
                var myshopify_domain = result[index].shopify_settings.shop.myshopify_domain;
                var domain = result[index].shopify_settings.shop.domain;
                domain = domain.substring(0, domain.length - 1);
                // if(myshopify_domain.indexOf('https') != -1){
                //     white_list.push(myshopify_domain);
                // }
                // if(domain.indexOf('https') != -1){
                //     white_list.push(domain);
                // }
                if(shop == myshopify_domain){
                    white_list.push('https://'+myshopify_domain);
                    return callback(white_list);
                }

            }
            return callback(white_list);
        }
        return callback([]);
    })
}

exports.getOriWebJSON = function (callback) {
    fs.readFile(path.join(__dirname, '../../../../public', 'website.json'), 'utf8', function (err, contents) {
        if (err) {
            return;
        }
        callback(JSON.parse(contents));
    });
}

exports.initUserInfo = function (obj) {
    var fingerPrint = obj.finger_print;
    var shop = obj.shop;
    if (UserController) {
        UserController.findUserByInfo(fingerPrint, shop, function (err, result) {
            if (err) return;
            if (!result) {
                UserController.createUser(obj, function (err1, result1) {
                })
            } else {
                UserController.updateUser(obj, function (err1, result1) {
                })

            }
        });
    }
}

exports.getPushContent = function (shop, type, callback) {
    PushController.getPushContent(shop, type, function (data) {
        callback(data);
    });
}

exports.getPayOffState = function (shop, callback) {
    PaymentController.getPayment(shop, function (data) {
        callback(data);
    });
}

exports.saveSetting = function (obj, callback) {
    if (isJsonString(obj)) obj = JSON.parse(obj);
    SettingController.saveSetting(obj, function () {
        callback();
    });
}

exports.getSetting = function (shop, callback) {
    SettingController.getSetting(shop, function (data) {
        callback(data);
    });
}

exports.getAllNotification = function (shop, callback) {
    NotificationController.getAllNotificationByShop(shop, callback);
}

exports.duplicateNotification = function (notification, req, callback) {
    if(isJsonString(notification)) {
        notification = JSON.parse(notification);
    }
    this.sendMessageToShop(notification, req, true, function () {
        callback();
    });
}

exports.clickNotification = function (notificationId) {
    NotificationController.getAllNotificationById(notificationId,function (notificationList) {
        if(notificationList.length>0){
            var notification = notificationList[0];
            notification.click+=1;
            NotificationController.updateNotification(notification);
        }
    });
}


exports.deleteNotification = function (notification, callback) {
    if(isJsonString(notification)){
        notification = JSON.parse(notification);
    }
    NotificationController.deleteNotification(notification._id,function () {
        callback();
    });
}

exports.savePushContent = function (obj) {
    PushController.savePushContent(obj);
}

exports.uploadImagePush = function (req, callback) {
    S3Provider.uploadImagePush(req, function (data) {
        callback(data);
    });
}

exports.uploadImageSetting = function (req, callback) {
    S3Provider.uploadImagePush(req, function (data) {
        callback(data);
    });
}

exports.createShopInfo = function (shop, info, tokenData, callback) {
    ShopController.createShop(shop, info, tokenData, callback);
}

exports.updateShopByWebhook = function (s3,shopInfo) {
    ShopController.updateShopByWebhook(shopInfo,function (item) {
        var shop = item.shop;
        S3Provider.changeDomain(shop,s3,function () {})
    });
}

exports.getShopInfo = function (shop,callback) {
    ShopController.getShopInfo(shop,callback);
}

exports.makeSubcription = function (shop,token,amount,title,callback) {
    var billing = new BillingApi(token,shop);
    var debugMode = process.env.SHOPIFY_STORM_PUSH_BILLING_DEBUG_STATE
    billing.createBill(title,amount,debugMode,function (err,info) {
        callback(JSON.parse(info).recurring_application_charge.confirmation_url);
    })
}

exports.getSubcription = function (shop,token,callback) {

    var billing = new BillingApi(token,shop);
    billing.getAllBilling(function (info) {
        callback(info);
    })
}

exports.getAllShopInfo = function (callback) {
    ShopController.getAllShop(shop, info, callback);
}

exports.initData = function (req, shop) {
    S3Provider.initImageSafari(req,shop);
}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function sendMessageApn(obj, tokenList,callback) {
    if (tokenList.length == 0) return;
    ApnProvider.sendMessage(tokenList, obj, function (numberSentSuccess) {
        callback(numberSentSuccess);
    });
}

function sendMessageFcm(obj, tokenList, req, callback) {
    if (tokenList.length == 0) return;
    var admin = req.app.get('firebaseAdmin');
    if (!admin) return;
    obj.enableFirstCTA = ""+obj.enableFirstCTA;
    obj.enableSecondCTA = ""+obj.enableSecondCTA;
    var temp = {shop: obj.shop,
        title: obj.title,
        description: obj.description,
        icon: obj.icon,
        image: obj.image,
        link: obj.link,
        firstCTAText: obj.firstCTAText,
        firstCTALink: obj.firstCTALink,
        firstCTAIcon: obj.firstCTAIcon,
        enableFirstCTA: ""+obj.enableFirstCTA,
        secondCTAText: obj.secondCTAText,
        secondCTALink: obj.secondCTALink,
        secondCTAIcon: obj.secondCTAIcon,
        enableSecondCTA: ""+obj.enableSecondCTA,
        type: obj.type,
        _id: ""+obj._id};
    FirebaseProvider.sendMessage(tokenList, temp, admin, function (numberSentSuccess) {
        callback(numberSentSuccess);
    });
}

function processSendPushMessage(listUser,obj,req,callback){
    var apnTokenList = [];
    var fcmTokenList = [];
    for (var i = 0; i < listUser.length; i++) {
        var token = listUser[i].token;
        var browser_name = listUser[i].browser_name;
        if (browser_name == MainServiceConstant.BROWSER_NAME_SAFARI) {
            apnTokenList.push(token);
        } else {
            fcmTokenList.push(token);
        }
        if (apnTokenList.length >= 20) {
            sendMessageApn(obj, apnTokenList,function (numberSentSuccess) {
                callback(numberSentSuccess);
            });
            apnTokenList = [];
        }
        if (fcmTokenList.length >= 20) {
            sendMessageFcm(obj, fcmTokenList, req,function (numberSentSuccess) {
                callback(numberSentSuccess);
            });
            fcmTokenList = [];
        }
    }
    if (apnTokenList.length > 0) {
        sendMessageApn(obj, apnTokenList,function (numberSentSuccess) {
            callback(numberSentSuccess);
        });
    }
    if (fcmTokenList.length > 0) {
        sendMessageFcm(obj, fcmTokenList, req,function (numberSentSuccess) {
            callback(numberSentSuccess);
        });
    }
}

exports.sendMessageToShop = function (obj, req, isDuplicate, duplicatedDoneCallback, finishSendPushMessageCallback) {
    UserController.getAllUser(obj.shop, function (listUser) {

        if (!isDuplicate) {
            obj.send = 0;
            obj.click = 0;
            obj.order = "";
            obj.revenueNumber = '';
            obj.revenueText = "";
            obj.repeat = 0;
            obj.createdAt = MainServiceUtil.getNow();
            NotificationController.createNotification(obj, function (notification) {
                finishSendPushMessageCallback(notification);
                processSendPushMessage(listUser,obj,req,function (numberSuccessSent) {
                    notification.send+=numberSuccessSent;
                    NotificationController.updateNotification(notification);
                });
            });
        } else {
            NotificationController.duplicateNotification(obj,function () {
                obj.send = 0;
                // obj.createdAt = MainServiceUtil.getNow();
                processSendPushMessage(listUser,obj,req,function (numberSuccessSent) {
                    obj.send+=numberSuccessSent;
                    NotificationController.updateNotification(obj);
                });
                duplicatedDoneCallback();
            })

        }
    })
}

exports.addScriptTag = function (api, clientUrl) {
    let scriptTagApi = new ScriptTagApi(api);
    scriptTagApi.list(function (list) {
        list = list.script_tags;
        var exist = false;
        for (var i = 0; i < list.length; i++) {
            var id = list[i].id;
            var src = list[i].src;
            if (src == clientUrl) {
                exist = true;
            } else {
                scriptTagApi.delete(id, function () {
                })
            }
        }
        if (!exist) {
            scriptTagApi.create(clientUrl, function (error1, result1, code) {
            })
        }
    })
}



exports.prepareFileForDownload = function (req, res) {
    fs.readFile("public/worker.js", function (err, data) {
        if (err) {
            res.statusCode = 500;
            res.end(`Error getting the file: ${err}.`);
        } else {
            // based on the URL path, extract the file extention. e.g. .js, .doc, ...
            // if the file is found, set Content-type and send data
            res.set('Service-Worker-Allowed', '/');
            res.setHeader('Content-type', 'text/javascript' || 'text/plain');
            res.end(data);
        }
    });
};

function getOriWebJSON(callback) {
    fs.readFile(path.join(__dirname, '../../../../public', 'website.json'), 'utf8', function (err, contents) {
        if (err) {
            console.error(err);
            return;
        }
        callback(JSON.parse(contents));
    });
}




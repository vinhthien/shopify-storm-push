'use strict';
var path = require('path');

module.exports = function (app, appslug) {
  // Root routing
  var coreController = require('../controllers/core.server.controller');

  var hrvApiMiddleware = require(path.join(__dirname, '../../../../share-lib/core/server/middlewares/haravan-countdown/core-haravan-api.server.middleware'));

  // install app
  // app.route('/http*+/').get(coreController.redirectSide);
  app.route('/'+appslug+'/backend/send-message-all').get(coreController.sendMessageSingle);
  app.route('/' + process.env.PATH_SHOPIFY_APP + '/' + appslug + '/install').get(coreController.install);
  // app.route('/v1/pushPackages/web.com.storm.push').get(coreController.ios);
  // app.route('/v1/pushPackages/web.com.storm.push').post(coreController.ios);
  // app.route('/v2/pushPackages/web.com.storm.push').post(coreController.ios);
  // app.route('/v2/pushPackages/web.com.storm.push').get(coreController.ios);
  // app.route('/v1/log').post(coreController.log);
  app.route('/'+appslug+'/backend/getUser').get(coreController.getUser);
  app.route('/'+appslug+'/getWebJson').get(coreController.getWebJSON);
  app.route('/'+appslug+'/shopWebhook').post(coreController.shopWebhook);
  app.route('/'+appslug+'/getShopInfo').get(coreController.getShopInfo);
  app.route('/'+appslug+'/makeSubcription').get(coreController.makeSubcription);
  app.route('/'+appslug+'/getSubcription').get(coreController.getSubcription);
  app.route('/'+appslug+'/uploadImagePush').post(coreController.uploadImagePush);
  app.route('/'+appslug+'/uploadImageSafariSetting').post(coreController.uploadImageSetting);
  app.route('/'+appslug+'/getPushContent').get(coreController.getPushContent);
  app.route('/'+appslug+'/saveSetting').get(coreController.saveSetting);
  app.route('/'+appslug+'/getSetting').get(coreController.getSetting);
  app.route('/'+appslug+'/getPayOffState').get(coreController.getPayOffState);
  app.route('/'+appslug+'/getAllNotification').get(coreController.getAllNotification);
  app.route('/'+appslug+'/initData').get(coreController.initData);
  app.route('/'+appslug+'/duplicateNotification').get(coreController.duplicateNotification);
  app.route('/'+appslug+'/deleteNotification').get(coreController.deleteNotification);
  app.route('/'+appslug+'/clickNotification').get(coreController.clickNotification);
  app.route('/app_proxy/'+appslug+'/get_worker').get(coreController.worker);
  app.route('/'+appslug+'/shopify').get(coreController.shopify);
  app.route('/'+appslug+'/shopify/callback').get(coreController.callback);
  //index view
  app.route('/' + process.env.PATH_HARAVAN_APP + '/' + appslug + '*').get(hrvApiMiddleware.haravanApi, coreController.renderIndex);

};

'use strict';

var path = require('path');
var mongoose = require('mongoose');
var url = require('url');
var crypto = require('crypto');
const fs = require('fs');
const nonce = require('nonce')();
var config = require(path.resolve('./config/config'));
var nlogger = require(path.resolve('./config/lib/nlogger'));
var ShopAPI = require(path.resolve('./apis/haravan/haravan-api/server/api/shop/shop.server.api'));
var ScriptTagApi = require(path.resolve('./apis/haravan/haravan-api/server/api/script-tag/script-tag.server.api'));
var AssetApi = require(path.resolve('./apis/haravan/haravan-api/server/api/asset/asset.server.api'));
var ThemeApi = require(path.resolve('./apis/haravan/haravan-api/server/api/theme/theme.server.api'));
// var ControllerShop = require(path.resolve('./embed-modules/shop/server/controllers/shop.server.controller'));
// var ControllerUser = require(path.resolve('./embed-modules/shop/server/controllers/user.server.controller'));
const express = require('express');
const webpush = require('web-push');
var ShopMD = mongoose.model(process.env.HARAVAN_DATABASE_PREFIX + '-' + 'Shop');
var DOWNLOAD_URL = "https://storm-push-safari.herokuapp.com";
// var DOWNLOAD_URL = "https://89bba909.ngrok.io/storm-push-safari";
var HARAVAN_APP_PORT = process.env.PORT;
var HARAVAN_API_KEY = process.env.HARAVAN_STORM_PUSH_API_KEY;
var HARAVAN_API_SECRET = process.env.HARAVAN_STORM_PUSH_API_SECRET;
var HARAVAN_SCOPE = process.env.HARAVAN_STORM_PUSH_API_SCOPE;
var REDIRECT_URI = process.env.WEB_HOST + '/' + process.env.PATH_HARAVAN_APP + '/' + process.env.HARAVAN_STORM_PUSH_APP_SLUG + '/finalize';
var AppService = require('../../../service/app.service');
var MainService = require('../../../../share-lib/service/main.service');

exports.layout = function (req, res) {
    res.render(path.join(__dirname, '../views/layoute'), {});
};

exports.getPushContent = function (req, res) {
    var shop = req.query.shop;
    var type = req.query.type;
    AppService.getPushContent(shop, type, function (data) {
        res.json({
            data: data
        });
    })
};

exports.saveSetting = function (req, res) {
    var obj = req.query;
    AppService.saveSetting(obj.chrome, function () {
        AppService.saveSetting(obj.firefox, function () {
            AppService.saveSetting(obj.safari, function () {
                res.json({});
            });
        });
    });
};

exports.getSetting = function (req, res) {
    var shop = req.query.shop;
    AppService.getSetting(shop, function (data) {
        res.json(data);
    });
};

exports.getPayOffState = function (req, res) {
    var shop = req.query.shop;
    AppService.getPayOffState(shop, function (data) {
        res.json(data);
    });
};

exports.getAllNotification = function (req, res) {
    var shop = req.query.shop;
    AppService.getAllNotification(shop, function (data) {
        res.json(data);
    });
};

exports.duplicateNotification = function (req, res) {
    var notification = req.query.notification;
    AppService.duplicateNotification(notification, req, function () {
        res.json({});
    });
};

exports.deleteNotification = function (req, res) {
    var notification = req.query.notification;
    AppService.deleteNotification(notification, function () {
        res.json({});
    });
};

exports.clickNotification = function (req, res) {
    var id = req.query.id;
    AppService.clickNotification(id);
};

/**
 * Render the embed application page
 */

exports.shopify = function (req, res) {
    console.log("IN");
    const shop = req.query.shop;
    if (shop) {
        const state = nonce();
        var forwardingAddress = process.env.WEB_HOST;
        const redirectUri = forwardingAddress + "/" + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + '/shopify/callback';
        const installUrl = 'https://' + shop +
            '/admin/oauth/authorize?client_id=' + process.env.SHOPIFY_STORM_PUSH_API_KEY +
            '&scope=' + process.env.SHOPIFY_STORM_PUSH_API_SCOPE +
            '&state=' + state +
            '&redirect_uri=' + redirectUri;

        res.cookie('state', state);
        res.redirect(installUrl);
    } else {
        res.render('embed-modules/core/server/views/install', {});
    }
};

exports.install = function (req, res) {

    const shop = req.query.shop;
    if (shop) {
        const url = process.env.WEB_HOST + "/" + process.env.SHOPIFY_STORM_PUSH_APP_SLUG + "/shopify?shop=" + shop + ".myshopify.com";
        res.redirect(url);
    } else {
        res.render(path.join(__dirname, '../views/install'), {});
    }
};


exports.callback = async function (req, res) {
    AppService.processAuthorize(req,res);
};


exports.renderIndex = async function (req, res) {
    console.log("render push")
    var access_token = req.session.access_token;
    var shop = req.query.shop;
    var store_id = req.session.store_id;
    var timestamp = req.query.timestamp || '';
    var signature = req.query.signature || '';
    var code = req.query.code || '';
    var embed = process.env.HARAVAN_STORM_PUSH_APP_SLUG;
    var embedUrl = config.protocol + shop + '/admin/app#/embed/' + HARAVAN_API_KEY;
    var domain = url.parse(config.apphost).hostname;
    if (!shop && !timestamp || !signature) {
        return res.sendStatus(401);
    }
    let tokenData = await getAccessToken(REDIRECT_URI, HARAVAN_API_KEY, HARAVAN_API_SECRET, code, req);
    if (tokenData) {
        AppService.checkScriptTag(HARAVAN_API_KEY, HARAVAN_API_SECRET, HARAVAN_SCOPE,
            REDIRECT_URI, code, shop, timestamp, signature, tokenData.access_token, HARAVAN_APP_PORT, config.protocol)
        AppService.getShopInfo(HARAVAN_API_KEY, HARAVAN_API_SECRET, HARAVAN_SCOPE,
            REDIRECT_URI, code, shop, timestamp, signature, tokenData.access_token, HARAVAN_APP_PORT, config.protocol, function (info) {
                AppService.createShopInfo(shop, info, tokenData, function () {

                })
            })
        var shopWebhookUrl = process.env.WEB_HOST + "/" + process.env.HARAVAN_STORM_PUSH_APP_SLUG + "/shopWebhook";
        AppService.initShopInfoWebhook(HARAVAN_API_KEY, HARAVAN_API_SECRET, HARAVAN_SCOPE,
            REDIRECT_URI, code, shop, timestamp, signature, tokenData.access_token, HARAVAN_APP_PORT, config.protocol,shopWebhookUrl);

    }
    res.render(path.join(__dirname, '../views/index'), {
        embed: embed,
        embedUrl: embedUrl,
        domain: domain,
        shopname: shop,
        timestamp: timestamp,
        signature: signature,
        code: code,
        access_token: access_token,
        store_id: store_id,
        public_key: req.app.get('public_key'),
        haravan_api: JSON.stringify(req.HaravanAPI),
        endpoint: process.env.AWS_S3_ENDPOINT
    });
};

exports.getUser = function (req, res) {
    var obj = req.query;
    AppService.initUserInfo(obj);
    res.status(200).json({});
};

exports.getWebJSON = function (req, res) {
    var shop = req.query.shop;
    AppService.getAllShop(shop,function (shopList) {
        AppService.getOriWebJSON(function (json) {
            json.allowedDomains = shopList;
            json.websiteName = 'https://'+shop;
            json.urlFormatString = shopList[0]+'/%@/';
            json.webServiceURL = shopList[0];
            res.json(json);
        });
    })
};

exports.shopWebhook = function (req, res) {
    var shopInfo = req.body;
    var s3 = req.app.get('s3Store');
    AppService.updateShopByWebhook(s3,shopInfo);
    res.json({});
};

exports.getShopInfo = function (req, res) {
    var shop = req.query.shop;
    AppService.getShopInfo(shop,function (shopInfo) {
        res.json(shopInfo);
    });

};

exports.makeSubcription = function (req, res) {
    var shop = req.query.shop;
    var accessToken = req.query.accessToken;
    var amount = req.query.amount;
    var title = req.query.title;
    AppService.makeSubcription(shop,accessToken,amount,title,function (info) {
        res.json(info);
    });
};

exports.getSubcription = function (req, res) {
    var shop = req.query.shop;
    var accessToken = req.query.token;
    AppService.getSubcription(shop,accessToken,function (info) {
        res.json(info);
    });

};

exports.sendMessageSingle = function (req, res) {
    var obj = req.query;
    AppService.sendMessageToShop(obj, req, false, null, function (notification) {
        res.json(notification);
    });
    AppService.savePushContent(obj);
};


exports.initData = function (req, res) {
    var shop = req.query.shop;
    AppService.initData(req, shop);
    res.json({});
};

exports.uploadImagePush = function (req, res) {
    AppService.uploadImagePush(req, function (data) {
        res.json(data);
    });
};

exports.uploadImageSetting = function (req, res) {
    AppService.uploadImageSetting(req, function (data) {
        res.json(data);
    });
};

/**
 * Render the server error page
 */
exports.renderServerError = function (req, res) {
    res.status(500).render('embed-modules/core/server/views/500', {
        error: 'Oops! Something went wrong...'
    });
};

/**
 * Render the server not found responses
 * Performs content-negotiation on the Accept HTTP header
 */
exports.renderNotFound = function (req, res) {

    res.status(404).format({
        'text/html': function () {
            res.render('embed-modules/core/server/views/404', {
                url: req.originalUrl
            });
        },
        'application/json': function () {
            res.json({
                error: 'Path not found'
            });
        },
        'default': function () {
            res.send('Path not found');
        }
    });
};

exports.ios = function (req, res) {
    console.log('shopify-storm-push')
    var shop = req.body.shop;
    var url = process.env.AWS_S3_ENDPOINT + shop + ".zip";
    res.redirect(url);
};

exports.log = function (req, res) {
    res.status(200).json({});
};

/**
 * Install application
 * @param req Incomming Request
 * @param res Response
 */


/**
 * Install application
 * @param req Incomming Request
 * @param res Response
 */
// exports.install = function (req, res) {
//     console.log("11")
//     if (req.query.shop) {
//         console.log("1111111");
//         var code = "codecode";
//         var timestamp = 6434632462;
//         var shop = req.query.shop + ".myharavan.com";
//         var secret = HARAVAN_API_SECRET;
//         var signer = crypto.createHmac('sha256', secret);
//         var str = '';
//         if (code) str = 'code=' + code;
//         str += 'shop=' + shop + 'timestamp=' + timestamp;
//         AppService.addShopAllow("https://" + shop);
//         var signature = signer.update(str).digest('hex');
//
//         res.redirect(url.format({
//             pathname: "/" + process.env.HARAVAN_STORM_PUSH_APP_SLUG + "/authenticate",
//             query: {
//                 shop: shop,
//                 timestamp: timestamp,
//                 signature: signature,
//                 code: code
//             }
//         }));
//
//     } else {
//         res.render(path.join(__dirname, '../views/install'), {});
//     }
//
// };


exports.worker = function (req, res) {

    fs.readFile("public/worker.js", function (err, data) {
        if (err) {
            res.statusCode = 500;
            res.end(`Error getting the file: ${err}.`);
        } else {
            // based on the URL path, extract the file extention. e.g. .js, .doc, ...
            // if the file is found, set Content-type and send data
            res.set('Service-Worker-Allowed', '/');
            res.setHeader('Content-type', 'text/javascript' || 'text/plain');
            res.end(data);
        }
    });

};


/**
 * Application Authenticate
 * @param req Incomming Request
 * @param res Response
 */
exports.authenticate = function (req, res) {
    let shop = req.query.shop || '';
    ShopMD.load(shop, function (err, shopData) {
        if (shopData && shopData.reintallapp) {

            res.render(path.join(__dirname, '../views/redirect')
                , {
                    auth_url: req.HaravanAPI.buildAuthURL(shop, HARAVAN_API_KEY, HARAVAN_SCOPE, REDIRECT_URI)
                });
        } else {
            res.redirect(req.HaravanAPI.buildAuthURL(shop, HARAVAN_API_KEY, HARAVAN_SCOPE, REDIRECT_URI));
        }
    });
};

/**
 * Finalized
 * @param req Incomming Request
 * @param res Response
 */
exports.finalize = async function (req, res) {
    console.log("askdhaoshdashdkajs")
    let shop = req.query.shop || '';
    let new_code = req.query.code;
    let timestamp = req.query.timestamp;
    let signature = req.query.signature;
    let code = req.query.code;
    let tokenData = await getAccessToken(REDIRECT_URI, HARAVAN_API_KEY, HARAVAN_API_SECRET, new_code, req);
    if (!tokenData) return res.sendStatus(401);
    var API = {
        haravan_api_key: HARAVAN_API_KEY,
        haravan_shared_secret: HARAVAN_API_SECRET,
        haravan_scope: HARAVAN_SCOPE,
        redirect_uri: REDIRECT_URI,
        code: code,
        shop: shop,
        timestamp: timestamp,
        signature: signature,
        access_token: tokenData.access_token,
        port: process.env.PORT,
        protocol: "https://"
    };
    req.HaravanAPI.config.access_token = tokenData.access_token;
    let shopAPI = new ShopAPI(req.HaravanAPI);
    var client_url = process.env.WEB_HOST + "/haravan-storm-push/lib/script-tags/push/js/storm-push-haravan-frontend.js";
    MainService.addScriptTag(API, client_url);

    let shopData = await getShopJson(shopAPI);
    if (!shopData) return res.sendStatus(401);

    var authorizeInfo = {
        access_token: tokenData.access_token,
        refresh_token: tokenData.refresh_token,
        expires_in: tokenData.expires_in,
        haravan_settings: shopData
    };
    var url = "?shop=" + shop + "&timestamp=" + timestamp + "&signature=" + signature + "&code=" + code;
    // ShopMD.updateAuthorize(shop, 1, authorizeInfo, function (newShopObj) {
    //     if (newShopObj && newShopObj._id) {
    //         try {
    //             return res.redirect('/' + process.env.PATH_HARAVAN_APP + '/' + process.env.STORM_PUSH_APP_SLUG + '/' + url);
    //         } catch (err) {
    //             if (err) {
    //                 nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
    //                     filename: __filename,
    //                     shop: req.HaravanAPI.config.shop
    //                 });
    //             }
    //             return res.sendStatus(401);
    //         }
    //     } else {
    //         return res.sendStatus(401);
    //     }
    // });
    AppService.createShop(shop, 1, authorizeInfo, function (newShopObj) {
        if (newShopObj && newShopObj.shop == shop) {
            try {
                return res.redirect('/' + process.env.PATH_HARAVAN_APP + '/' + process.env.HARAVAN_STORM_PUSH_APP_SLUG + '/' + url);
            } catch (err) {
                if (err) {
                    nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
                        filename: __filename,
                        shop: req.HaravanAPI.config.shop
                    });
                }
                return res.sendStatus(401);
            }
        } else {
            return res.sendStatus(401);
        }
    })
};

async function getAccessToken(redirect_uri, haravan_api_key, haravan_shared_secret, code, req) {
    return new Promise((resolve, reject) => {
        req.HaravanAPI.getCustomNewAccessToken(redirect_uri, haravan_api_key, haravan_shared_secret, code, function (err, tokenData) {
            if (err) {
                nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
                    filename: __filename,
                    fn: 'getNewAccessToken',
                    shop: req.HaravanAPI.config.shop
                });
                resolve();
            } else if (tokenData && tokenData.access_token) {
                resolve(tokenData);
            } else {
                resolve();
            }
        });
    });
};

async function getShopJson(shopAPI) {
    return new Promise((resolve, reject) => {
        shopAPI.get(function (err, shopData) {
            if (err) {
                nlogger.writelog(nlogger.NLOGGER_ERROR, err, {
                    filename: __filename,
                    fn: 'getShopJson',
                    shop: shopAPI.config.shop
                });
                resolve();
            } else if (shopData && shopData.id) {
                resolve(shopData);
            } else {
                resolve();
            }
        });
    });
};

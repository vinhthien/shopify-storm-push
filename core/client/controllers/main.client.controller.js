'use strict';
angular.module('core').controller('MainController', ['$scope',
    '$location', '$state', 'Menus', '$http', 'Notification', '$translate', '$window', '$rootScope',
    function ($scope, $location, $state, Menus, $http, Notification, $translate, $window, $rootScope) {
        console.log("in view push")

        // Expose view variables
        $scope.domain = '';
        $scope.shop = '';
        $scope.key = '';
        $scope.canShow = false;
        $scope.endpoint = '';
        $scope.titleHolder = 'Example: New Notification';
        $scope.titleHolderPreview = 'New Notification';
        $scope.descriptionHolder = 'Example: New promotions';
        $scope.descriptionHolderPreview = 'New promotions';
        $scope.hasPayOff = false;
        $scope.master = {
            shop: "",
            title: "",
            description: "",
            icon: $scope.defaultIcon,
            image: "images/410_2.jpg",
            link: "",
            firstCTAText: "",
            firstCTALink: "",
            firstCTAIcon: $scope.defaultIcon,
            enableFirstCTA: false,
            secondCTAText: "",
            secondCTALink: "",
            secondCTAIcon: $scope.defaultIcon,
            enableSecondCTA: false,
            type: ""
        }
        $scope.safariIcon = '';

        $scope.setvalue = function (name, value) {
            $scope.master[name] = value + "?" + new Date().getTime();
            // $scope.master[name] = $scope.endpoint + value;
            $scope.$apply();
        }

        function loadSetting() {
            var data = {shop: $scope.master.shop};
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/getSetting", config).then(function (response) {
                if(response.data.length == 0){
                    $scope.safariIcon = $window.getFavicon($scope.safari.shop);
                    return;
                }
                angular.forEach(response.data, function (value, key) {
                    if (value.browser.toLowerCase().indexOf("safari") != -1) {
                        $scope.safariIcon = value.icon;
                    }
                });
            });
        }

        function fetchData(shop, type) {
            var data = {
                shop: shop,
                type: type
            };
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/getPushContent", config).then(function (response) {
                var data = response.data.data;
                if (data._id && data._id.length > 0) {
                    delete data['__v'];
                    delete data['_id'];
                    $scope.master = data;
                    if ($scope.master.firstCTAText.length > 0 && $scope.master.firstCTALink.length > 0) {
                        $scope.enableFirstCTA = true;
                    }
                    if ($scope.master.secondCTALink.length > 0 && $scope.master.secondCTAText.length > 0) {
                        $scope.enableSecondCTA = true;
                    }
                }
            });
        }



        $scope.init = function (type) {
            $scope.master.shop = $window.shopname;
            $scope.master.type = type;
            $scope.endpoint = $window.endpoint;
            // $scope.master.icon = $window.getFavicon($scope.master.shop);
            // $scope.master.firstCTAIcon = $window.getFavicon($scope.master.shop);
            // $scope.master.secondCTAIcon = $window.getFavicon($scope.master.shop);
            // fetchData($scope.master.shop, $scope.master.type);
            loadSetting();
            loadShopInfo($scope.master.shop);
        }



        function loadShopInfo(shop){

            var data = {shop: shop};
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/getShopInfo", config).then(function (response) {
                if(response.data){
                    $scope.master.link = response.data.shopify_settings.shop.domain;
                    $scope.master.firstCTALink = response.data.shopify_settings.shop.domain;
                    $scope.master.secondCTALink = response.data.shopify_settings.shop.domain;
                }

            });

        }

        $scope.changeCTA = function (bool) {
            if (bool) {
                if (!$scope.master.enableFirstCTA) {
                    $scope.master.enableFirstCTA = bool;
                } else if (!$scope.master.enableSecondCTA)
                    $scope.master.enableSecondCTA = bool;
            } else {
                if ($scope.master.enableSecondCTA) {
                    $scope.master.enableSecondCTA = bool;
                } else if ($scope.master.enableFirstCTA)
                    $scope.master.enableFirstCTA = bool;
            }
        }
        $scope.sendMessage = function () {
            if(!validateBeforeSend()){
                alert("Missing info");
                return;
            }
            var data = $scope.master;


            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/backend/send-message-all", config).then(function (response) {
                $window.alert(
                    "Sent successfull");
                $rootScope.$emit("addNewPushMessageToReport", response.data);
            });
        }

        function validateBeforeSend() {
            if($scope.master.title.length == 0){
                return false;
            }
            if($scope.master.description.length == 0){
                return false;
            }
            if($scope.master.link.length == 0){
                return false;
            }
            if($scope.master.enableFirstCTA){
                if($scope.master.firstCTAText.length == 0){
                    return false;
                }
                if($scope.master.firstCTALink.length == 0){
                    return false;
                }
            }
            if($scope.master.enableSecondCTA){
                if($scope.master.secondCTAText.length == 0){
                    return false;
                }
                if($scope.master.secondCTALink.length == 0){
                    return false;
                }
            }
            if($scope.master.type == "push-sale"){
                $scope.master.image = '';
            }
            return true;
        }
    }
]);

angular.module('core').controller('PushFrameController', ['$scope',
    '$location', '$state', 'Menus', '$http', 'Notification', '$translate', '$window', '$rootScope',
    function ($scope, $location, $state, Menus, $http, Notification, $translate, $window, $rootScope) {
        console.log("in push frame")
        $scope.view = '0';
        $scope.changeView = function (viewName) {
            $scope.view = viewName;
        }
        $rootScope.$on("goMainPage", function () {
            $scope.view = '0';
        });

    }]);

angular.module('core').controller('FrameController', ['$scope',
    '$location', '$state', 'Menus', '$http', 'Notification', '$translate', '$window', '$rootScope',
    function ($scope, $location, $state, Menus, $http, Notification, $translate, $window, $rootScope ) {
        $scope.menuPos = 1;
        $scope.mainTitle = '';
        $scope.childTitle = '';
        $scope.hasPayOff = false;
        $scope.defaultIcon = $window.defaultIcon;
        $scope.username = $window.user;
        $scope.shopUrl = $window.shopUrl;

        function getTranslateText(id,callback){
            $translate(id).then(function (translatedString) {
                callback(translatedString);
            });
        }

        $scope.showPaymentPage = function(){
            $scope.changeMenu(5);
        }

        $scope.changeMenu = function (position) {
            $scope.menuPos = position;
            if (position == 1) {
                $rootScope.$emit("goMainPage", {});
            }

            if(position == 1){
                getTranslateText("create_campaign",function (text) {
                    $scope.childTitle = text;
                })
            }else if(position == 2){
                getTranslateText("report_campaign",function (text) {
                    $scope.childTitle = text;
                })
            }else if(position == 3){
                getTranslateText("setting",function (text) {
                    $scope.childTitle = text;
                })
            }else if(position == 4){
                getTranslateText("About",function (text) {
                    $scope.childTitle = text;
                })
            }else if(position == 5){
                getTranslateText("Payment",function (text) {
                    $scope.childTitle = text;
                })
            }
        }

        function changeLang(lang) {
            $translate.use(lang);
        }

        changeLang('push-en');
        $scope.init = function () {
            setTimeout(function () {
                hideSplashPage();
                getTranslateText("push_notification",function (text) {
                    $scope.mainTitle = text;
                })
                getTranslateText("create_campaign",function (text) {
                    $scope.childTitle = text;
                })
                loadSubcription($window.shopname,$window.access_token);
                // loadPayoffState($window.shopname);

            }, 1000);

        }

        $scope.getPayOff = function () {
            return $scope.hasPayOff;
        }

        function loadSubcription(shop,token) {
            var data = {shop: shop,token: token};
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/getSubcription", config).then(function (response) {
                if(response.data){
                    var listSubcription = JSON.parse(response.data).recurring_application_charges;
                    if(listSubcription.length==0){
                        return;
                    }
                    var status = listSubcription[0].status;
                    if(status === "active"){
                        $scope.hasPayOff = true;
                    }
                }
            });
        }

        function loadPayoffState(shop) {
            var data = {shop: shop};
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/getPayOffState", config).then(function (response) {
                if(response.data){
                    console.log("response.data")
                    console.log(response.data)
                    $scope.hasPayOff = response.data[0].hasPayOff;
                }

            });
        }

        function hideSplashPage() {
            $window.showContent();
        }

    }]);

angular.module('core').controller('ReportController', ['$scope',
    '$location', '$state', 'Menus', '$http', 'Notification', '$translate', '$window', '$rootScope', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$interval',
    function ($scope, $location, $state, Menus, $http, Notification, $translate, $window, $rootScope, DTOptionsBuilder, DTColumnDefBuilder, $interval) {
        $scope.notificationList = [];
        $scope.number = 0;
        $scope.intervalTime = 5000;
        $scope.loadDataList = function (callback) {
            $http.get("/" + $window.embed + "/getAllNotification?shop=" + $window.shopname)
                .then(function (response) {
                    response.data = sortArrayData(response.data);
                    for (var i = 0; i < response.data.length; i++) {
                        response.data[i].createdAt = convertDate(response.data[i].createdAt);
                    }
                    $scope.notificationList = response.data;
                    $scope.number = $scope.notificationList.length;
                    if(callback) callback();

                });
        }

        function convertDate(date) {
            return moment(date, 'YYYY-MM-DDTHH:mm:ssZ').format('DD/MM/YYYY HH:mm:ss');
            ;
        }

        function sortArrayData(array){
            let _sortedDates = array.sort(function(a, b){
                return moment(b.createdAt, 'YYYY-MM-DDTHH:mm:ssZ').format('X')-moment(a.createdAt, 'YYYY-MM-DDTHH:mm:ssZ').format('X')
            });
            return _sortedDates;
        }

        function sortArrayDataCustom(array){
            let _sortedDates = array.sort(function(a, b){
                return moment(b.createdAt, 'DD/MM/YYYY HH:mm:ss').format('X')-moment(a.createdAt, 'DD/MM/YYYY HH:mm:ss').format('X')
            });
            return _sortedDates;
        }

        $scope.init = function(){
            $scope.loadDataList(function () {
            });
            $interval(function() {
                syncData()
            }, $scope.intervalTime);
        }
        function syncData(){
            $http.get("/" + $window.embed + "/getAllNotification?shop=" + $window.shopname)
                .then(function (response) {
                    response.data = sortArrayData(response.data);
                    for (var index = 0; index < response.data.length; index++) {
                        response.data[index].createdAt = convertDate(response.data[index].createdAt);
                        var newData = response.data[index];
                        for (var alpha = 0; alpha < $scope.notificationList.length; alpha++) {
                            var oldData = $scope.notificationList[alpha];
                            if(oldData._id == newData._id){
                                if(oldData.send != newData.send){
                                    oldData.send = newData.send;
                                }
                                if(oldData.click != newData.click){
                                    oldData.click = newData.click;
                                }
                                if(oldData.order != newData.order){
                                    oldData.order = newData.order;
                                }
                                if(oldData.revenueNumber != newData.revenueNumber){
                                    oldData.revenueNumber = newData.revenueNumber;
                                }
                                if(oldData.repeat != newData.repeat){
                                    oldData.repeat = newData.repeat;
                                }
                                if(oldData.createdAt != newData.createdAt){
                                    oldData.createdAt = newData.createdAt;
                                }
                                break;
                            }
                        }

                    }
                    $scope.notificationList = sortArrayDataCustom($scope.notificationList);
                });
        }
        $rootScope.$on("addNewPushMessageToReport", function (event, notification) {
            notification.createdAt = convertDate(notification.createdAt);
            $scope.notificationList.unshift(notification);
        });

        $scope.duplicateNotification = function (index) {
            duplicateNotification(index);
        }

        $scope.deleteNotification = function (index) {
            deleteNotification(index);
        }

        function deleteNotification(index) {
            $scope.notificationList[index].enableDelete = true;
            var obj = $scope.notificationList[index];
            var data = {notification: obj};
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/deleteNotification", config).then(function (response) {
                $window.alert("successfull");
                $scope.notificationList.splice(index, 1);
            });
        }

        function getNow() {
            return moment().format();
        }

        function duplicateNotification(index) {
            $scope.notificationList[index].enableDuplicate = true;
            var obj = $scope.notificationList[index];
            obj.createdAt = getNow();
            var data = {notification: obj};
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };
            $http.get("/" + $window.embed + "/duplicateNotification", config).then(function (response) {
                $window.alert("successfull");
                $scope.notificationList[index].enableDuplicate = false;
                $scope.notificationList[index].repeat+=1;
                $scope.notificationList[index].createdAt=convertDate(getNow());
            });
        }
    }]);

angular.module('core').controller('SettingController', ['$scope',
    '$location', '$state', 'Menus', '$http', 'Notification', '$translate', '$window', '$rootScope',
    function ($scope, $location, $state, Menus, $http, Notification, $translate, $window, $rootScope) {
        console.log("in setting")
        $scope.chrome = {
            enableGDPR: false,
            title: "Welcome to website",
            description: "To get more information please register",
            acceptButton: "Accept",
            declineButton: "Decline",
            shop: "",
            browser: "chrome",
            icon: $scope.defaultIcon
        };
        $scope.firefox = {
            enableGDPR: false,
            title: "Welcome to website",
            description: "To get more information please register",
            acceptButton: "Accept",
            declineButton: "Decline",
            shop: "",
            browser: "firefox",
            icon: $scope.defaultIcon
        };
        $scope.safari = {
            enableGDPR: true,
            title: "Welcome to website",
            description: "To get more information please register",
            acceptButton: "Accept",
            declineButton: "Decline",
            shop: "",
            browser: "safari",
            icon: $scope.defaultIcon
        };
        $scope.init = function () {
            $scope.chrome.shop = $window.shopname;
            $scope.firefox.shop = $window.shopname;
            $scope.safari.shop = $window.shopname;
            loadSetting(function () {
            });
        }
        $scope.changeSafariIcon = function (url) {
            $scope.safari.icon = url + "?"+ new Date().getTime();
        }

        $scope.changeChromeIcon = function (url) {
            $scope.chrome.icon = url + "?"+ new Date().getTime();
        }

        $scope.changeFirefoxIcon = function (url) {
            $scope.firefox.icon = url + "?"+ new Date().getTime();
        }


        function loadSetting(callback) {
            var data = {shop: $scope.chrome.shop};
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            $http.get("/" + $window.embed + "/getSetting", config).then(function (response) {
                if(response.data.length == 0){
                    // $scope.safari.icon = $window.getFavicon($scope.safari.shop);
                    // $scope.firefox.icon = $window.getFavicon($scope.safari.shop);
                    // $scope.chrome.icon = $window.getFavicon($scope.safari.shop);
                    initData(function () {
                        $scope.save(function () {
                        })
                    });
                    return;
                }
                angular.forEach(response.data, function (value, key) {
                    delete value['__v'];
                    delete value['_id'];
                    if (value.browser == $scope.chrome.browser) {
                        $scope.chrome = value;
                    } else if (value.browser == $scope.firefox.browser) {
                        $scope.firefox = value;
                    }else if (value.browser == $scope.safari.browser) {
                        $scope.safari = value;
                    }
                });
                callback();
            });
        }

        function initData(callback){
            var data = $scope.safari;
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };
            $http.get("/" + $window.embed + "/initData", config).then(function (response) {
                callback();
            });
        }

        $scope.save = function (callback) {
            if ($scope.chrome.enableGDPR) {
                if ($scope.chrome.title.length == 0 || $scope.chrome.description.length == 0
                    || $scope.chrome.title.length == 0 || $scope.chrome.title.length == 0) {
                    $window.alert("chrome browser missing info");
                    return;
                }
            }

            if ($scope.firefox.enableGDPR) {
                if ($scope.firefox.title.length == 0 || $scope.firefox.description.length == 0
                    || $scope.firefox.title.length == 0 || $scope.firefox.title.length == 0) {
                    $window.alert("firefox browser missing info");
                    return;
                }
            }

            $window.saveImage(function () {
                var data = {chrome: $scope.chrome, firefox: $scope.firefox, safari: $scope.safari};
                var config = {
                    params: data,
                    headers: {'Accept': 'application/json'}
                };

                $http.get("/" + $window.embed + "/saveSetting", config).then(function (response) {
                    if(!callback){
                        $window.alert("save successfull");
                    }
                });
            });
        }
    }]);


'use strict';

// Setting up route
angular
    .module('core')
    .config(routesConfig);

routesConfig.$inject = [
  '$stateProvider',
  '$urlRouterProvider'
];

function routesConfig($stateProvider, $urlRouterProvider) {
  // Redirect to 404 when route not found
  $urlRouterProvider.otherwise(function ($injector, $location) {
    $injector
        .get('$state')
        .transitionTo('not-found', null, {
          location: false
        });
  });
    var path = '/haravan-storm-push';
    var name = 'push'
  // Home state routing
  $stateProvider
      .state(name+'home', {
        url: '/' + appslug + '/',
        controller: 'HomeContsasaroller'
      })
      .state(name+'not-found', {
        url: '/' + appslug + '/not-found',
        templateUrl: path + '/core/client/views/404.client.view.html',
        data: {
          ignoreState: true,
          pageTitle: 'Not-Found'
        }
      })
      .state(name+'bad-request', {
        url: '/' + appslug + '/bad-request',
        templateUrl: path + '/core/client/views/400.client.view.html',
        data: {
          ignoreState: true,
          pageTitle: 'Bad-Request'
        }
      })
      .state(name+'forbidden', {
        url: '/' + appslug + '/forbidden',
        templateUrl: path + '/core/client/views/403.client.view.html',
        data: {
          ignoreState: true,
          pageTitle: 'Forbidden'
        }
      })
      .state(name+'server-error', {
        url: '/' + appslug + '/server-error',
        templateUrl: path + '/core/client/views/500.client.view.html',
        data: {
          ignoreState: true,
          pageTitle: 'Server Error'
        }
      });
}

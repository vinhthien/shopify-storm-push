(function () {
  'use strict';

  angular
  .module('core')
  .run(MenuConfig);

  MenuConfig.$inject = ['Menus'];

  function MenuConfig(Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Cấu hình',
      state: 'settings'
    });
  }

})();

'use strict';

angular
    .module('core')
    .factory('ToastMessage', ToastMessage);

ToastMessage.$inject = [
  '$window'
];

function ToastMessage($window) {
  return {
    info: info,
    error: error,
    success: success
  };

  function _show(type, message) {
    if($window.parent) {
      $window.parent.postMessage({
        functionname: 'toastr',
        type: type,
        message: message
      }, '*');
    }
  }

  function info(message) {
    _show('Info', message);
  }

  function error(message) {
    _show('Error', message);
  }

  function success(message) {
    _show('Success', message);
  }
}

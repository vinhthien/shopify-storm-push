class FirebaseAdmin {

    static sendMessage(tokenList,obj,callback){
        var apn = require('apn');
        var path = require('path');
        let service = new apn.Provider({
            cert: path.join(__dirname, '../../../../public/cert/', 'cert.pem'),
            key: path.join(__dirname, '../../../../public/cert/', 'key.pem'),
            passphrase:"",
            production: true //use this when you are using your application in production.For development it doesn't need
        });

        if(obj.link.indexOf('https') == -1 && obj.link.indexOf('http') == -1){
            obj.link = 'https://'+obj.link;
        }

        let note = new apn.Notification({
                "aps": {
                    "alert": {
                        "title": obj.title,
                        "body": obj.description,
                        "action": "Read more"
                    },
                    "url-args": [obj.link]
                }
            }
        );

        service.send(note, tokenList).then( result => {//ios key is holding array of device ID's to which notification has to be sent
            console.log("sent:", result.sent.length);
            console.log("failed:", result.failed.length);
            console.log(result.failed);
            console.log(result);
            callback(result.sent.length);

        });
    }
}

module.exports = FirebaseAdmin;
class FirebaseAdmin {

    static sendMessage(tokenList,obj,admin,callback){
        if(!admin) return;
        // var payload = {
        //     "notification": {
        //         "title": title,
        //         "body": description,
        //         "click_action": redirectTo,
        //         "icon": icon
        //     }
        // };
        var payload = {
            data: obj
        };
        if(payload.data.link.indexOf('http') == -1 || payload.data.link.indexOf('https') == -1){
            payload.data.link = 'https://'+payload.data.link;
        }
        admin.messaging().sendToDevice(tokenList, payload)
            .then(function(response) {
                console.log('Successfully sent message:', response);
                callback(response.successCount);
            })
            .catch(function(error) {
                console.log('Error sending message:', error);
                console.log(error);
            });
    }
}

module.exports = FirebaseAdmin;
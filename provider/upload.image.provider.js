class S3Provider {


    static createIconSafari(originalKey, shop, app, s3, callback) {
        setTimeout(function () {
            var prefix = shop + "_";
            var icon_1 = prefix + 'icon_16x16.png';
            var icon_2 = prefix + 'icon_16x16@2x.png';
            var icon_3 = prefix + 'icon_32x32.png';
            var icon_4 = prefix + 'icon_32x32@2x.png';
            var icon_5 = prefix + 'icon_128x128.png';
            var icon_6 = prefix + 'icon_128x128@2x.png';
            S3Provider.deleteObj(icon_1, s3);
            S3Provider.deleteObj(icon_2, s3);
            S3Provider.deleteObj(icon_3, s3);
            S3Provider.deleteObj(icon_4, s3);
            S3Provider.deleteObj(icon_5, s3);
            S3Provider.deleteObj(icon_6, s3);
            S3Provider.resizeImage(originalKey, 16, 16, icon_1, s3, function () {
                S3Provider.resizeImage(originalKey, 32, 32, icon_2, s3, function () {
                    S3Provider.resizeImage(originalKey, 32, 32, icon_3, s3, function () {
                        S3Provider.resizeImage(originalKey, 64, 64, icon_4, s3, function () {
                            S3Provider.resizeImage(originalKey, 128, 128, icon_5, s3, function () {
                                S3Provider.resizeImage(originalKey, 256, 256, icon_6, s3, function () {
                                    setTimeout(function () {
                                        S3Provider.uploadZip(shop, app, s3, function () {
                                            if(callback){
                                                callback();
                                            }
                                        });
                                    }, 5000);
                                })
                            })
                        })
                    })
                })
            });
        }, 3000);
    }

    static deleteObj(key, s3) {
        var params = {Bucket: process.env.AWS_S3_BUCKET, Key: key};
        s3.deleteObject(params, function (err, data) {
            if (err) console.log(err, err.stack);  // error
            else console.log();                 // deleted
        });
    }

    static uploadZip(shop, app, s3, callback) {
        console.log('app');
        console.log(app);
        const request = require('request')
        var url = "https://storm-push-safari.herokuapp.com";
        // var url = "https://211f6736.ngrok.io/storm-push-safari";
        request({
            url: url + "?shop=" + shop + "&date=" + new Date().getTime() + "&app=" + app,
            encoding: null
        }, function (err, res, body) {
            if (err)
                return callback(err, res);

            s3.putObject({
                Bucket: process.env.AWS_S3_BUCKET,
                Key: shop + ".zip",
                ContentType: res.headers['content-type'],
                ContentLength: res.headers['content-length'],
                Body: body // buffer
            }, callback);
        })
    }

    static changeDomain(shop,s3){
        S3Provider.uploadZip(shop,s3,function () {

        })
    }

    static createOrigicalSafariPackage(shop, app, s3, callback) {
        const request = require('request')
        // var url = "https://storm-push-safari.herokuapp.com/original.php";
        var url = "https://storm-push-safari.herokuapp.com/";
        // var url = "https://211f6736.ngrok.io/storm-push-safari";
        request({
            url: url + "?shop=" + shop + "&date=" + new Date().getTime() + "&app=" + app,
            encoding: null
        }, function (err, res, body) {
            if (err)
                return callback(err, res);

            s3.putObject({
                Bucket: process.env.AWS_S3_BUCKET,
                Key: shop+"-original" + ".zip",
                ContentType: res.headers['content-type'],
                ContentLength: res.headers['content-length'],
                Body: body // buffer
            }, callback);
        })

    }

    static resizeImage(originalKey, witdh, height, name, s3, callback) {
        const Sharp = require('sharp');
        s3.getObject({Bucket: process.env.AWS_S3_BUCKET, Key: originalKey}).promise()
            .then(data => Sharp(data.Body)
                .resize(witdh, height)
                .toFormat('png')
                .toBuffer()
            )
            .then(buffer => s3.putObject({
                    Body: buffer,
                    Bucket: process.env.AWS_S3_BUCKET,
                    ContentType: 'image/png',
                    Key: name,
                }).promise()
            )
            .then(() => callback(null)
            )
            .catch(err => callback(err))
    }

    static uploadImagePush(req, callback) {
        var s3 = req.app.get('s3Store');
        const fileName = req.query['file-name'];
        const fileType = req.query['file-type'];
        const shop = req.query['shop'];
        const type = req.query['type'];
        var fileEx = fileType.split("/")[1];
        var name = shop + "." + type + "." + fileEx;
        const s3Params = {
            Bucket: process.env.AWS_S3_BUCKET,
            Key: name,
            Expires: 60,
            ContentType: fileType,
            ACL: 'public-read'
        };

        s3.getSignedUrl('putObject', s3Params, (err, data) => {
            if (err) {
                return callback(null);
            }
            const returnData = {
                signedRequest: data,
                url: process.env.AWS_S3_ENDPOINT + name
            };
            setTimeout(function () {
                if (type.indexOf("safari") != -1) {
                    S3Provider.createIconSafari(name, shop, process.env.SHOPIFY_STORM_PUSH_APP_SLUG, s3);
                }
            }, 1000);
            callback(returnData);

        });
    }


    static initImageSafari(req, shop) {
        var s3 = req.app.get('s3Store');
        const request = require('request')
        var url = req.query.icon;
        console.log("req.query");
        console.log(req.query);
        var name = shop + ".safari-icon.png";
        request({
            url: url,
            encoding: null
        }, function (err, res, body) {
            if (err) {
                return;
            }

            s3.putObject({
                Bucket: process.env.AWS_S3_BUCKET,
                Key: name,
                Expires: 60,
                ContentType: "image/png",
                ACL: 'public-read',
                Body: body // buffer
            }, function () {
                setTimeout(function () {
                    S3Provider.createIconSafari(name, shop, process.env.SHOPIFY_STORM_PUSH_APP_SLUG, s3, function () {
                        S3Provider.createOrigicalSafariPackage(shop, process.env.SHOPIFY_STORM_PUSH_APP_SLUG, s3,function () {})
                    });
                }, 2000);

            });
        })


    }
}

module.exports = S3Provider;
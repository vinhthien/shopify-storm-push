function send_all($window) {
    var shop = $window.shopname;
    var access_token = $window.access_token;
    var title = $('#title').val();
    var description = $('#description').val();
    var redirectTo = $('#redirectTo').val();
    $.ajax({
        url: "/"+embed+"/backend/send-message-all",
        type: "get", //send it through get method
        data: {
            shop: shop,
            title: title,
            description: description,
            redirectTo: redirectTo
        },
        contentType: "application/json",
        success: function (response) {//success
            if(!response.err){
                alert("Send successfull");
                angular.element('#ReportController').scope().loadDataList();
                angular.element('#ReportController').scope().$apply()
            }else {
                alert("Send error");
            }
        }
    });
}

function choose_icon(id) {
    // $("#icon").click();
    var hasPayOff = angular.element(document.getElementById('frame')).scope().getPayOff();
    if(!hasPayOff){
        $('#paymentButton').click();
        return;
    }
    document.getElementById(id).click();
}

function initUpload(input,img,angularValue){
    const files = document.getElementById(input).files;
    const file = files[0];
    if(file == null){
        return alert('No file selected.');
    }
    getSignedRequest(file,input,img,angularValue);
}

function getSignedRequest(file,input,img,angularValue){
    const xhr = new XMLHttpRequest();
    xhr.open('POST', `/storm-push/uploadImagePush?file-name=${file.name}&file-type=${file.type}&shop=${this.shopname}&type=${input}`);
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4){
            if(xhr.status === 200){
                const response = JSON.parse(xhr.responseText);
                uploadFile(file, response.signedRequest, response.url, img ,angularValue);
            }
            else{
                alert('Could not get signed URL.');
            }
        }
    };
    xhr.send();

}

function getFavicon(shop) {
    return "https://www.google.com/s2/favicons?domain="+shop;
}

function uploadFile(file, signedRequest, url, img, angularValue){
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', signedRequest);
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4){
            if(xhr.status === 200){
                $("#"+img).attr("src",url);
                if(document.getElementById('sale-push')){
                    angular.element(document.getElementById('sale-push')).scope().setvalue(angularValue,url);
                }else if(document.getElementById('power-push')){
                    angular.element(document.getElementById('power-push')).scope().setvalue(angularValue,url);
                    setTimeout(function() {
                        $("#"+img).attr("src",url+"?"+new Date().getTime());
                    }, 1000);
                }
            }
            else{
                alert('Could not upload file.');
            }
        }
    };
    xhr.send(file);
}

function changeIcon(input,img,angularValue){
    initUpload(input,img,angularValue);
}


// var url_storm_push_shopify = "https://d646bf61.ngrok.io/";
var url_storm_push_shopify = "./apps/shopify-storm-push/";
var shop = Shopify.shop;
var pushId = "web.com.storm.push";
var browserName = '';
var deferreds = [];
var jsFiles = [];

$('head').append('<script src="'+ url_storm_push_shopify + 'jquery/dist/jquery.min.js"></script>');
$('head').append('<script src="'+ url_storm_push_shopify + 'bootstrap/dist/js/bootstrap.min.js"></script>');
$('head').append('<script src="'+ url_storm_push_shopify + 'remarkable-bootstrap-notify/bootstrap-notify.js"></script>');
$('head').append('<script src="'+ url_storm_push_shopify + 'pgwbrowser/pgwbrowser.js"></script>');
$('head').append('<script src="'+ url_storm_push_shopify + 'fingerprint/fingerprint.js"></script>');
$('head').append('<script src="'+ url_storm_push_shopify + 'fingerprintjs2/fingerprint2.js"></script>');
$('head').append('<script src="'+ url_storm_push_shopify + 'moment/moment.js"></script>');
jsFiles.push(url_storm_push_shopify+"jquery/dist/jquery.min.js");
jsFiles.push(url_storm_push_shopify+"bootstrap/dist/js/bootstrap.js");
jsFiles.push(url_storm_push_shopify+"remarkable-bootstrap-notify/bootstrap-notify.js");
jsFiles.push(url_storm_push_shopify+"pgwbrowser/pgwbrowser.js");
jsFiles.push(url_storm_push_shopify+"fingerprint/fingerprint.js");
jsFiles.push(url_storm_push_shopify+"fingerprintjs2/fingerprint2.js");
jsFiles.push("https://www.gstatic.com/firebasejs/4.6.2/firebase.js");
$.each(jsFiles, function (idx, url) {
    deferreds.push(jQuery.ajax({
        type: "GET",
        url: url,
        dataType: "script",
        cache: true
    }));
});

deferreds.push($.Deferred(function (deferred) {
    $(deferred.resolve);
}));

$.when.apply(this, deferreds).done(function () {
    var pgwBrowser = $.pgwBrowser();
    browserName = pgwBrowser.browser.name;
    if(pgwBrowser.browser.name == "Safari"){
        get_permission_safari(pushId,url_storm_push_shopify,shop,browserName);
    }else {
        if (!("Notification" in window)) {
            return;
        }
        if (Notification.permission == 'granted' || Notification.permission == "denied") {
            return;
        }
        getSetting(function (list) {
            if(list.length == 0){
                startRegisterServiceWorker();
                return;
            }
            list.forEach(function(value) {
                delete value['__v'];
                delete value['_id'];
                if (browserName.toLocaleLowerCase().indexOf(value.browser) != -1) {
                    var enableGDPR = value.enableGDPR;
                    var acceptButton = value.acceptButton;
                    var declineButton = value.declineButton;
                    var description = value.description;
                    var title = value.title;
                    var icon = value.icon;
                    if(enableGDPR){
                        prepareHTML(title,description,acceptButton,declineButton,icon);
                        setTimeout(function() {
                            $("#storm-push").modal('show');
                        }, 1000);
                    }else {
                        startRegisterServiceWorker();
                    }
                }
            });

        });





    }

});

function prepareHTML(title,description,acceptButton,declineButton,icon) {
    $('head').append('<link id="styles-boostrap" rel="stylesheet" type="text/css" href="'+url_storm_push_shopify+'assets/global/css/bootstrap.min.css">');
    $('head').append(' <link href="'+url_storm_push_shopify+'shopify-storm-push/lib/html/css/front-end-modal.css" rel="stylesheet" type="text/css"/>');
    $('body').append(
        '<div class="modal fade" tabindex="-1" role="dialog" id="storm-push">' +
        '  <div class="modal-dialog">' +
        '    <div class="modal-content row">' +
        '      <div class="modal-body">' +
        '        <div class="title"></div>       ' +
        '        <div class="des"></div>       ' +
        '           <img src="'+icon+'">' +
        '        <div class="row button-group">' +
        '           <a data-dismiss="modal" class="ng-binding decline-button"></a>' +
        '           <button class="ng-binding accept-button" data-dismiss="modal" onclick="startRegisterServiceWorker()"></button>' +
        '        </div>       ' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>');
    $("#storm-push").find(".title").text(title);
    $("#storm-push").find(".des").text(description);
    $("#storm-push").find('.button-group').find('.accept-button').text(acceptButton);
    $("#storm-push").find('.button-group').find('.decline-button').text(declineButton);
    $('#storm-push').on('hidden.bs.modal', function () {
        // do something…
        $('link[src="'+url_storm_push_shopify+'assets/global/css/bootstrap.min.css"]').remove();

        var stylesheet = document.getElementById('styles-boostrap');
        stylesheet.parentNode.removeChild(stylesheet);

    })
}

function startRegisterServiceWorker() {
    register_service_worker(function (registration) {
        init(registration,browserName);
    })
}

function getSetting(callback) {
    $.ajax({
        url: url_storm_push_shopify + "shopify-storm-push/getSetting",
        type: "get", //send it through get method
        data: {
            "shop": shop
        },
        contentType: "application/json",
        success: function (response) {//success
            callback(response);
        }
    });
}

function register_service_worker(callback) {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/apps/shopify-storm-push/shopify-storm-push-sw.js', { scope: '/apps/shopify-storm-push/' }).then(function(registration) {
            console.log('Service Worker registration completed with scope: ',
                registration.scope)
            callback(registration);
        }, (err) => {
            console.log('Service Worker registration failed', err)
        })
    }
}

function renew_service_worker(navigator,callback) {
    navigator.serviceWorker.getRegistrations().then(function(registrations) {
        for(let registration of registrations) {
            console.log(registration);
            var a = registration;
            var url = registration.active.scriptURL;
            if(url.indexOf("storm-push-haravan-sw.js") != -1){
                registration.unregister();
                break;
            }
        }
        callback();
    })


}

function init(reg,browser_name) {
    var config = {
        apiKey: "AIzaSyBycgcYBw0MizHdYXInX24pUo3ZigBXoqo",
        authDomain: "storm-push-notification.firebaseapp.com",
        databaseURL: "https://storm-push-notification.firebaseio.com",
        projectId: "storm-push-notification",
        storageBucket: "storm-push-notification.appspot.com",
        messagingSenderId: "680906819828"
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    messaging.useServiceWorker(reg);
    messaging
        .requestPermission()
        .then(function () {
            console.log("Notification permission granted.");

            // get the token in the form of promise
            return messaging.getToken()
        })
        .then(function(token) {
            console.log("token is : " + token);
            init_user_info(shop,token,browser_name);
        })
        .catch(function (err) {
            console.log("Unable to get permission to notify.", err);
        });

    messaging.onMessage(function(payload) {
        console.log("payload")
        console.log(payload)
        alert("have message");
        // var obj = payload.notification;
        // var title = obj.title;
        // var body = obj.body;
        // var url = obj.click_action;
        // var icon = obj.icon;
        // $.notify({
        //     icon: icon,
        //     title: title,
        //     message: body,
        //     url: url,
        //     target: '_blank'
        // },{
        //     type: 'minimalist',
        //     delay: 5000,
        //     icon_type: 'image',
        //     template: '<div style="background-color: rgb(241, 242, 240);' +
        //     '    border-color: rgba(149, 149, 149, 0.3);' +
        //     '    border-radius: 3px;' +
        //     '    color: rgb(149, 149, 149);' +
        //     '    padding: 10px;" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
        //     '<img  style=" height: 50px;' +
        //     '    margin-right: 12px;" data-notify="icon" class="img-circle pull-left">' +
        //     '<span style="color: rgb(51, 51, 51);' +
        //     '    display: block;' +
        //     '    font-weight: bold;' +
        //     '    margin-bottom: 5px;" data-notify="title">{1}</span>' +
        //     '<span style="    font-size: 80%;"  data-notify="message">{2}</span>' +
        //     '</div>'
        // });
    });
}


function init_user_info(shop,token,browser_name) {
    get_browser_id(function (fingerprint) {
        get_user_info(token,fingerprint,shop,browser_name,function (response) {

        })
    })
}

function check() {
    
}

function update_identify(token,fingerprint,shop,browser_name,callback) {
    var url = url_storm_push_shopify + "baseapp/embed/api/updateUser" + "?finger_print=" + fingerprint + "&shop=" + shop + "&token=" + token + "&browser_name="+ browser_name;
    $.post(url, function (data) {
        callback(data);
    }, "json");
}
function create_identify(token,fingerprint,shop,browser_name,callback) {
    var url = url_storm_push_shopify + "baseapp/embed/api/createUser" + "?finger_print=" + fingerprint + "&shop=" + shop + "&token=" + token + "&browser_name="+ browser_name;
    $.post(url, function (data) {
        callback(data);
    }, "json");
}



function is_safari() {
    if ('safari' in window && 'pushNotification' in window.safari) {
        return true
    }
    return false;
}


function get_permission_safari(pushId,url_server,shop,browser_name) {
    if ('safari' in window && 'pushNotification' in window.safari) {
        url_server = url_server.substring(0, url_server.length-1);
        var permissionData = window.safari.pushNotification.permission(pushId);
        checkRemotePermission(permissionData,url_server,pushId,shop,browser_name);
    }
}

function get_browse_info(callback) {
    if (window.requestIdleCallback) {
        requestIdleCallback(function () {
            Fingerprint2.get(function (components) {
                callback(components);
            })
        })
    } else {
        setTimeout(function () {
            Fingerprint2.get(function (components) {
                callback(components);
            })
        }, 500)
    }
}

function get_browser_id(callback) {
    var fingerprint = new Fingerprint().get();
    callback(fingerprint);
}

function checkRemotePermission(permissionData,server_url,push_id,shop,browser_name) {
    if (permissionData.permission === 'default') {
        console.log("The user is making a decision");
        window.safari.pushNotification.requestPermission(
            server_url,
            push_id,
            {shop: shop},
            checkRemotePermission
        );
    }else if (permissionData.permission === 'denied') {
        console.dir(arguments);
    }else if (permissionData.permission === 'granted') {
        console.log("The user said yes, with token: "+ permissionData.deviceToken);
        init_user_info(shop,permissionData.deviceToken,browserName)
    }
};

function get_user_info(token,fingerprint,shop,browser_name,callback) {
    $.ajax({
        url: url_storm_push_shopify + "shopify-storm-push/backend/getUser",
        type: "get", //send it through get method
        data: {
            "finger_print": fingerprint,
            "shop": shop,
            "token": token,
            "browser_name": browser_name
        },
        contentType: "application/json",
        success: function (response) {//success
            callback(response);
        }
    });
}


// if (('serviceWorker' in navigator)) {
//     // Service Worker isn't supported on this browser, disable or hide UI.
//     alert("serviceWorker");
// }
//
// if (('PushManager' in window)) {
//     // Push isn't supported on this browser, disable or hide UI.
//     alert("PushManager");
// }
function choose_icon(id) {
    var hasPayOff = angular.element(document.getElementById('frame')).scope().getPayOff();
    if(!hasPayOff){
        $('#paymentButton').click();
        return;
    }
    document.getElementById(id).click();
}

function initUploadSetting(input,name,callback){
    const files = document.getElementById(input).files;
    const file = files[0];
    if(file == null){
        return callback();
    }
    getSignedRequestSetting(file,name,callback);
}

function getSignedRequestSetting(file,name,callback){
    const xhr = new XMLHttpRequest();
    xhr.open('POST', `/shopify-storm-push/uploadImageSafariSetting?file-name=${file.name}&file-type=${file.type}&shop=${this.shopname}&type=${name}`);
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4){
            if(xhr.status === 200){
                const response = JSON.parse(xhr.responseText);
                uploadFileSetting(file, response.signedRequest, response.url, callback);
            }
            else{
                alert('Could not get signed URL.');
            }
        }
    };
    xhr.send();
}

function uploadFileSetting(file, signedRequest, url, callback){
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', signedRequest);
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4){
            if(xhr.status === 200){
                callback(url);
            }
            else{
                alert('Could not upload file.');
            }
        }
    };
    xhr.send(file);
}

function getFavicon(shop) {
    return "https://www.google.com/s2/favicons?domain="+shop;
}

function saveImage(callback) {
    changeIconSafari(function () {
        changeIconChrome(function () {
            changeIconFirefox(function () {
                callback();
            });        
        });    
    });
    
    
}

function readURL(inputId,img) {
    var input = $("#"+inputId)[0];
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('.'+img).attr('src', e.target.result);
            if(inputId.toLowerCase().indexOf("safari") != -1){
                // $('.'+img).attr('background', e.target.result);
                // $('.safari-small-icon').css('background-image', 'none');
                // $('.safari-small-icon').css('background', 'none');
                // $('.safari-small-icon').attr('background', e.target.result);
                $('.safari-small-icon').css("background-image", "url('" + e.target.result + "')");

            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}


function changeIconSafari(callback){
    initUploadSetting("safari-input","safari-icon",function (url) {
        if(url){
            angular.element(document.getElementById('setting')).scope().changeSafariIcon(url);
        }
        callback();
    });
}

function changeIconChrome(callback){
    initUploadSetting("chrome-input","chrome-icon",function (url) {
        if(url) {
            angular.element(document.getElementById('setting')).scope().changeChromeIcon(url);
        }
        callback();
    });
}
function changeIconFirefox(callback){
    initUploadSetting("firefox-input","firefox-icon",function (url) {
        if(url) {
            angular.element(document.getElementById('setting')).scope().changeFirefoxIcon(url);
        }
        callback();
    });
}

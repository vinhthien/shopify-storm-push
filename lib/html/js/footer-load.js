var url = window.location.origin + "/";
var deferreds = [];
var jsFiles = [];
jsFiles.push(url+"assets/global/plugins/respond.min.js");
jsFiles.push(url+"assets/global/plugins/excanvas.min.js");
jsFiles.push(url+"assets/global/plugins/jquery.min.js");
jsFiles.push(url+"assets/global/plugins/jquery-migrate.min.js");
jsFiles.push(url+"assets/global/plugins/jquery-ui/jquery-ui.min.js");
jsFiles.push(url+"assets/global/plugins/bootstrap/js/bootstrap.min.js");
jsFiles.push(url+"assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");
jsFiles.push(url+"assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");
jsFiles.push(url+"assets/global/plugins/jquery.blockui.min.js");
jsFiles.push(url+"assets/global/plugins/jquery.cokie.min.js");
jsFiles.push(url+"assets/global/plugins/uniform/jquery.uniform.min.js");
jsFiles.push(url+"assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");
jsFiles.push(url+"assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js");
jsFiles.push(url+"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js");
jsFiles.push(url+"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js");
jsFiles.push(url+"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js");
jsFiles.push(url+"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js");
jsFiles.push(url+"assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js");
jsFiles.push(url+"assets/global/plugins/flot/jquery.flot.min.js");
jsFiles.push(url+"assets/global/plugins/flot/jquery.flot.resize.min.js");
jsFiles.push(url+"assets/global/plugins/flot/jquery.flot.categories.min.js");
jsFiles.push(url+"assets/global/plugins/jquery.pulsate.min.js");
jsFiles.push(url+"assets/global/plugins/bootstrap-daterangepicker/moment.min.js");
jsFiles.push(url+"assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js");
jsFiles.push(url+"assets/global/plugins/fullcalendar/fullcalendar.min.js");
jsFiles.push(url+"assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js");
jsFiles.push(url+"assets/global/plugins/jquery.sparkline.min.js");
jsFiles.push(url+"assets/global/scripts/metronic.js");
jsFiles.push(url+"assets/admin/layout/scripts/layout.js");
jsFiles.push(url+"assets/admin/layout/scripts/quick-sidebar.js");
jsFiles.push(url+"assets/admin/layout/scripts/demo.js");
jsFiles.push(url+"assets/admin/pages/scripts/index.js");
jsFiles.push(url+"assets/admin/pages/scripts/tasks.js");
$.each(jsFiles, function (idx, url) {
    // deferreds.push(jQuery.ajax({
    //     type: "GET",
    //     url: url,
    //     dataType: "script",
    //     cache: true
    // }));
    $("body").append('<script src="'+url+'"></script>');
    Metronic.init(); // init metronic core componets
    Layout.init(); // init layout
    QuickSidebar.init(); // init quick sidebar
});

